use std::{error::Error, fmt};

use serde::Deserialize;
use serde_json::Value;

pub mod client;
mod config;

#[derive(Deserialize)]
struct BybitResult {
    pub ret_code: u16,   // error code 0 means success
    pub ret_msg: String, // error message
    pub ext_code: String,
    pub ext_info: String,
    pub result: Value,
    pub time_now: String,

    // POST result only
    #[serde(default)]
    pub rate_limit_status: u16,
    #[serde(default)]
    pub rate_limit_reset_ms: u64,
    #[serde(default)]
    pub rate_limit: u16,
}

impl BybitResult {
    pub fn has_error(&self) -> bool {
        self.ret_code != 0
    }

    pub fn get_error(&self) -> BybitError {
        BybitError {
            er_code: self.ret_code,
            er_msg: self.ret_msg.clone(),
            ex_code: self.ext_code.clone(),
            ex_msg: self.ext_info.clone(),
        }
    }
}

struct BybitError {
    pub er_code: u16,
    pub er_msg: String,
    pub ex_code: String,
    pub ex_msg: String,
}

impl Error for BybitError {}

impl fmt::Display for BybitError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut er_msg = format!("{} - {}", self.er_code, self.er_msg);
        if !self.ex_code.is_empty() {
            er_msg.push_str(&format!("\n\t{} - {}", self.ex_code, self.ex_msg));
        }
        write!(f, "{}", er_msg)
    }
}

impl fmt::Debug for BybitError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut er_msg = format!("{} - {}", self.er_code, self.er_msg);
        if !self.ex_code.is_empty() {
            er_msg.push_str(&format!("\n\t{} - {}", self.ex_code, self.ex_msg));
        }
        write!(f, "Kraken Error: {}", er_msg)
    }
}
