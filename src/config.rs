pub const API_URL: &str = "https://api.bybit.com";
pub const API_VER: &str = "/v2";

pub const API_PUBLIC: &str = "/public";
pub const API_PRIVATE: &str = "/private";

