use std::error::Error;

use serde_json::{json, Value};

use crate::{
    config::{API_PRIVATE, API_PUBLIC, API_URL, API_VER},
    BybitResult,
};

enum RequestMethod {
    GET,
    POST,
}

pub struct BybitClient {
    api_key: Option<String>,
    api_secret: Option<String>,
    time_offset: i64,
}

impl BybitClient {
    pub fn new() -> Self {
        Self {
            api_key: None,
            api_secret: None,
            time_offset: 0,
        }
    }

    pub fn with_credentials(api_key: String, api_secret: String) -> Self {
        Self {
            api_key: Some(api_key),
            api_secret: Some(api_secret),
            time_offset: 0,
        }
    }

    pub async fn sync_time(&mut self) -> Result<(), Box<dyn Error>> {
        let json = self.server_time().await?;
        if let Some(time) = json["server_time"].as_str() {
            let server_time = (time.parse::<f64>()? * 1000.0) as i64;
            let current_time = chrono::Utc::now().timestamp_millis();
            self.time_offset = server_time - current_time;
        }
        Ok(())
    }

    pub async fn server_time(&self) -> Result<Value, Box<dyn Error>> {
        let uri = format!("{}{}{}/time", API_URL, API_VER, API_PUBLIC);
        let result = surf::get(uri).recv_json::<BybitResult>().await?;
        if result.has_error() {
            Err(Box::new(result.get_error()))
        } else {
            Ok(json!({"server_time": result.time_now}))
        }
    }

    pub async fn post(&self, endpoint: &str, payload: Value) -> Result<Value, Box<dyn Error>> {
        self.api_request(endpoint, Some(payload), true, RequestMethod::POST)
            .await
    }

    pub async fn get(
        &self,
        endpoint: &str,
        payload: Option<Value>,
        is_private: bool,
    ) -> Result<Value, Box<dyn Error>> {
        self.api_request(endpoint, payload, is_private, RequestMethod::GET)
            .await
    }

    async fn api_request(
        &self,
        endpoint: &str,
        payload: Option<Value>,
        is_private: bool,
        method: RequestMethod,
    ) -> Result<Value, Box<dyn Error>> {
        let uri = format!(
            "{}{}{}{}",
            API_URL,
            API_VER,
            if is_private { API_PRIVATE } else { API_PUBLIC },
            endpoint
        );

        let payload = match payload {
            Some(v) => v,
            None => json!({}),
        };

        let request = match method {
            RequestMethod::GET => surf::get(uri).query(&payload)?,
            RequestMethod::POST => surf::post(uri).body(payload),
        };

        let result = request.recv_json::<BybitResult>().await?;
        if result.has_error() {
            Err(Box::new(result.get_error()))
        } else {
            Ok(result.result)
        }
    }

    /// Get a reference to the bybit client's time offset.
    pub fn time_offset(&self) -> &i64 {
        &self.time_offset
    }
}
